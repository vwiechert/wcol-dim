Report on
NOWHERE DENSE GRAPH CLASSES AND DIMENSION
+++++++++++++++++++++++++++++++++++++++++++++++++

Streib and Trotter proved that there is a function f
such that any poset P with a planar cover graph and height h
has dim(P) \leq f(h). In subseqent work this kind of
result was obtained for larger classes of posets, in particular
for posets whose cover graph
� have bounded treewidth, bounded genus, or more generally
  exclude an apex-graph as minor [JMMTWW-Combinatorica2016];
� exclude a fixed graph as a (topological) minor [W-JCTB2017,MW-JGT2017];
� belong to a fixed class with bounded expansion [JMW-Combinatorica2018];

In [JMW-Combinatorica2018] it was observed that
posets of height 2 whose cover graphs are nowhere dense
have unbounded dimension.

The main result of the present paper is
Theorem 1
  A monotone class of graphs C is nowhere dense if and only
  if for every integer h > 1 and real number e > 0,
  n-element posets of height at most h with cover graphs in C
  have dimension O(n^e).

The key idea for the proof is to closely relate dimension
and weak coloring numbers. The connection is
established with Theorem 3 which reproves the result
about bounded expansion with a better bounding function.
As corollaries new improved bounds are given for some other
classes. In the case of treewidth the exponential bound is shown to be
essentially optimal. The construction of the corresponding posets
$P_{h,t}$ is very nice. The proof given for Theorem 1 in Section 4
is rather long and technical. The future may bring substantial
simplifications but at least the result is established for now.

RECOMMENDATION: I strongly recommend accepting this
paper -in a revised version- for publication in Combinatorica.

Regarding the revision I have some minor comments below.
My most substantial critique is that the paper is partially
written in a foggy style. E.g. for the proof in Sec 4
it would be helpful to see an idea before diving into the
details. On the web I found a set of slides by one
of the authors:
http://orderandgeometry2016.tcs.uj.edu.pl/docs/OG2016-Lecture-Wiechert
These slides helped me a lot in getting the
picture. They explicitly state two theorems of Zhu relating graph classes
with weak coloring numbers. Also the diagrams are very helpful.
Why not reproduce an updated version of slide 45 or a table
presenting the data in the paper?

Detail comments:

p5l8 that - than

p10l6 is as to - is as far to

p20l-14 the cover graph G_Q of Q is an (induced) subgraph of G
     - add the explanation:  since Q is an up-set of P

I recommend to integrate both footnotes in the running text.

p22l-11 Furthermore - We claim that

p22l-8 then at most - then after at most

For the second = in the display we need that S \leq y for all
y which qualify for being added to V and, hence, R \leq y for all y

p22l-1 here and on the following page it should always be D[z]
instead of D(z)
